# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 21:37:41 2022

@author: syn
mathisjoly6@gmail.com
"""

import time

class Spaceship :

    def __init__(self,name = 'ufo',coord = [0,0]):
        self. name = name
        self.speed = 0
        self.coords = coord
        self.direction = [0,0]
        self.in_move = False
            
    def getName(self):
        return self.name

    def getPosition(self):
        return self.coords

    def setVectorDirection(self,vectorDirection =[0,0]):
        self.direction = vectorDirection
                    
    def  actualisation(self,vectorDirection=[0,0],interval = 1.0):
        self.coords = [self.coords[0]+vectorDirection[0],self.coords[1]+vectorDirection[1]]
        time.sleep(interval)

    def start(self,vectorDirection = [0,0],interval =1.0,flytime = 1):
        self.setVectorDirection(vectorDirection)

        for i in range(flytime):
            self.actualisation(vectorDirection,interval)

    def stop (self):
        self.setVectorDirection([0,0])



