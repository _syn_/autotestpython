# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 09:17:06 2022

@author: syn
mathisjoly6@gmail.com
"""

import unittest
from Spaceship import Spaceship
from threading import Timer




class TestMethods(unittest.TestCase):
#    def __init__(self):
#        pass


    def test_name(self):
        ship = Spaceship('SlaveI')
        self.assertEqual(ship.getName(), 'SlaveI')

    def test_default_name(self):
        ship = Spaceship()
        self.assertEqual(ship.getName(), 'ufo')

    def test_get_position(self):
        ship = Spaceship('SlaveI',[0,0])
        self.assertEqual(ship.getPosition(), [0, 0])

    def test_defaults_position(self):
        ship = Spaceship('SlaveI')
        self.assertEqual(ship.getPosition(), [0, 0])

    def test_position_after_move(self):
        ship = Spaceship('SlaveI',[0,0])
        self.assertEqual(ship.getPosition(), [0, 0])
        
        Timer(5,ship.stop()).start()
        ship.start([10,0],1,5)
        self.assertEqual(ship.getPosition(),[50,0])


if __name__ == '__main__':
    unittest.main()
#    input()